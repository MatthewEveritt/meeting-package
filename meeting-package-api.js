/*
    File: meeting-package-api.js

    Description: 
    This api requires the host name(website name) and room ID from the client.
    It will check whether the venue that the room belongs to is available on the website 
    that the request came from.
    If the venue is visible it will return the price per hour for that room based on the website.
    If the venue is not visible on the website then the api will return a suitable message.

    Parameters: 
    rooms - pointing to the collection in the database named rooms
    roomId - id of the room being requested
    
    Returns:
    room price - price of room per hour based on which website the price was requested from
    venue availability - returns a message if venue is not available on the website
*/

//Import dependencies
import Express, { json, urlencoded } from "express";
import { MongoClient } from "mongodb";
import { ObjectID as ObjectId } from "mongodb";

const CONNECTION_URL = "mongodb://localhost:27017/meeting_package";
const DATABASE_NAME = "meeting_package";

//Initialize the Express framework
const app = Express();
app.use(json());
app.use(urlencoded({ extended: true }));

//Initialize global variables to be used in all endpoints
const database, collection;

//Establishing connection with MongoDB database
app.listen(8080, () => {
    MongoClient.connect(CONNECTION_URL, { usedNewUrlParser: true }, (error, client) => {
        if(error) {
            throw error;
        }
        database = client.db(DATABASE_NAME);
        collection = database.collection("rooms");
        console.log("Connected to `" + DATABASE_NAME + "`!");
     });
});

//Rooms endpoint to return price per room to website
app.get("/rooms/:id", (request, response) => {
    const website = request.get('host'); // Assign website domain 

    //Getting room object from database by ObjectId
    collection.findOne({ "_id": new ObjectId(request.params.id) }, (error, result) => {
        
        //Returns error response if database returns error
        if(error) {
            return response.status(500).send(error);
        }

        //Checks if rooms venue is set to true for given website
        if(result.websiteVisibility[website] == true ) {
            const pricePerHour = result.pricePerWebsite[host];//Assigns price per hour based on website
            response.send(`The price per hour for this room is €${pricePerHour}`);//Returns price per hour message
        } else {

            //If rooms venue is not set to true, returns unavailable message
            response.send('This venue is not available on this website.')
        }
    });
});
